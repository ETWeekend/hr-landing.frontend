import { Validate } from "@components/forms/Validate";
import { SingleSelect } from "@components/select";
import { popupThanks, popupError } from "@components/popup";

popupThanks.init();
popupError.init();
const options = {
    selector: "[data-form-validate]",
    requiredClasses: ".required",
    errorClass: "sm-validate__error",
    errorMessage: "data-field-error-message",
    errorSelector: "[data-field-error]",
    phoneMask: {
        mask: "+{7}(000)000-00-00",
    },
    isPhoneInput: "input[type='tel']",
    parent: "[data-field]",
    lang: "ru-RU",
    send: (el, data) => {
        const request = new XMLHttpRequest();

        request.open("POST", el.getAttribute("action"), true);

        request.processData = false;
        request.contentType = false;
        request.onreadystatechange = function (event) {
            if (event.status === 4 || event.status === 200) {
                popupThanks.openPopup(document.querySelector("[data-popup-thanks]"));
            } else {
                popupError.openPopup(document.querySelector("[data-popup-error]"));
            }
        };
        request.send(data);
    },
    success: (el) => {},
    error: (el) => {
        console.error(el, " error");
    },
    selectArr: [
        {
            id: 0,
            value: "Копирайтером",
        },
        {
            id: 1,
            value: "Аккаунт-менеджером",
        },
        {
            id: 2,
            value: "Другая вакансия",
        },
    ],
};

new SingleSelect("[data-select-vacancy-footer]", {
    placeholder: "Выберите из списка",
    template: {
        container: `
          <div class="select__backdrop" data-type="backdrop"></div>
          <div class="select__input" data-type="input">
            <input type="hidden" required="required" class="required" data-field-error-message="Не выбрано кем хотите работать" name="vacancy" value="{{selected}}" />
            <div class="select__value select__value--placeholder" data-type="value">{{value}}</div>
            <div class="select__arrow" data-type="input">
                <svg width="15" height="15" viewBox="0 0 15 15" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path fill-rule="evenodd" clip-rule="evenodd" d="M13.6061 6.63762C14.1243 7.15581 14.1243 7.99596 13.6061 8.51415L8.54446 13.5758C8.02628 14.094 7.18613 14.094 6.66794 13.5758L1.60627 8.51415C1.08809 7.99596 1.08809 7.15581 1.60627 6.63762C2.12446 6.11944 2.96461 6.11944 3.4828 6.63762L7.6062 10.761L11.7296 6.63762C12.2478 6.11944 13.0879 6.11944 13.6061 6.63762Z" fill="white"/>
                </svg>
            </div>
            <span class="select__error" data-field-error="true"></span>
          </div>
          <div class="select__dropdown">
            <ul class="select__list">
              {{items}}
            </ul>
          </div>
        `,
        item: `<li class="select__item {{className}}" data-type="item" data-id="{{id}}">{{value}}</li>`,
    },
    data: options.selectArr,
    onSelect: () => {
        forms.formValidate(document.querySelector("[data-form-validate]"));
    },
});

window.forms = new Validate(options);
