import { getScrollbarWidth } from "@utils/helpers";
import { isMobile } from "@utils/devices";

class Popup {
    constructor(props) {
        this.props = props;

        this.popup = props.popup;
        this.trigger = props.trigger;
        this.close = "[data-popup-close]";
        this.header = document.querySelector("[data-header]");
        this.openedClass = props.openedClass || "opened";
        this.onDocument = props.onDocument;
        this.lockScroll = props.lockScroll;
        this.form = props.form;
        this.onlyMob = props.onlyMob;
    }

    init() {
        if (this.onlyMob !== undefined && this.onlyMob && !isMobile()) return;

        const popup = document.querySelector(this.popup);
        if (!popup) return;

        const closes = popup.querySelectorAll(this.close);

        closes.forEach((close) => {
            close.addEventListener("click", () => {
                this.closePopup(popup);
            });
        });

        document.addEventListener("keyup", (event) => {
            if (event.key === "Escape" || event.key === "Esc") {
                if (popup.classList.contains(this.openedClass)) {
                    this.closePopup(popup);
                }
            }
        });

        if (this.onDocument) {
            document.addEventListener("click", (event) => {
                const trigger = event.target.closest(this.trigger);
                if (trigger) this.openPopup(popup, trigger);
            });
        } else {
            const triggers = document.querySelectorAll(this.trigger);
            triggers.forEach((trigger) => {
                trigger.addEventListener("click", () => {
                    this.openPopup(popup, trigger);
                });
            });
        }

        if (this.form) {
            const form = popup.querySelector("[data-validate-form]");
            if (form) {
                const inputs = form.querySelectorAll("input");
                form.addEventListener("isValid", () => {
                    this.closePopup(popup);
                    inputs.forEach((input) => {
                        input.value = "";
                    });
                });
            }
        }

        if (this.props.onOpen !== undefined && this.popup !== undefined) {
            popup.addEventListener("openPopup", this.props.onOpen);
        }

        if (this.props.onClose !== undefined && this.popup !== undefined) {
            popup.addEventListener("popupClose", this.props.onClose);
        }
    }

    openPopup(popup, trigger) {
        popup.dispatchEvent(new CustomEvent("openPopup", { detail: { popup: popup, trigger: trigger } }));

        if (this.lockScroll) {
            const scrollBarWidth = getScrollbarWidth();
            if (this.header) this.header.style.right = `${scrollBarWidth}px`;
            document.documentElement.style.marginRight = `${scrollBarWidth}px`;
            document.documentElement.style.overflow = "hidden";
        }
        popup.classList.add(this.openedClass);
    }

    closePopup(popup) {
        popup.dispatchEvent(new CustomEvent("popupClose", { detail: { popup: popup } }));

        if (this.lockScroll) {
            if (this.header) this.header.style.removeProperty("right");
            document.documentElement.style.removeProperty("margin-right");
            document.documentElement.style.removeProperty("overflow");
        }
        popup.classList.remove(this.openedClass);
    }

    onOpen(callback) {
        document.querySelector(this.popup).addEventListener("openPopup", callback, { once: true });
    }

    onClose(callback) {
        document.querySelector(this.popup).addEventListener("popupClose", callback, { once: true });
    }
}

export default Popup;
