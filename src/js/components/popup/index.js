import Popup from "@components/popup/Popup";
import PushText from "@components/PushText";
import { SingleSelect } from "@components/select";

const popupStory = new Popup({
    popup: "[data-popup-story]",
    trigger: "[data-popup-story-trigger]",
    lockScroll: true,
    onOpen: (event) => {
        const popup = event.detail.popup;
        const trigger = event.detail.trigger;
        const storyID = trigger.getAttribute("data-story-id");
        if (!storyID) return;
        const storyContainer = popup.querySelector("[data-story-container]");

        fetch(`./stories/${storyID}.html`)
            .then((response) => {
                if (response.ok) {
                    return response.text();
                } else {
                    throw Error("Error request");
                }
            })
            .then((result) => {
                storyContainer.innerHTML = result;
            })
            .then(() => {
                const pushTextItems = popup.querySelectorAll("[data-animate-push-text]");
                if (pushTextItems.length) {
                    new PushText({
                        elements: pushTextItems,
                        pauseTime: 2000,
                    });
                }
            });
    },
    onClose: (event) => {
        const popup = event.detail.popup;
        const storyContainer = popup.querySelector("[data-story-container]");
        storyContainer.innerHTML = "";
    },
});

const selectArr = [
    {
        id: 0,
        value: "Копирайтером",
    },
    {
        id: 1,
        value: "Аккаунт-менеджером",
    },
    {
        id: 2,
        value: "Другая вакансия",
    },
];

const vacancySelect = new SingleSelect("[data-select-vacancy-popup]", {
    placeholder: "Выберите из списка",
    template: {
        container: `
          <div class="select__backdrop" data-type="backdrop"></div>
          <div class="select__input" data-type="input">
            <input type="hidden" required="required" class="required" data-field-error-message="Не выбрано кем хотите работать" name="vacancy" value="{{selected}}" />
            <div class="select__value select__value--placeholder" data-type="value">{{selected}}</div>
            <div class="select__arrow" data-type="input">
                <svg width="15" height="15" viewBox="0 0 15 15" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path fill-rule="evenodd" clip-rule="evenodd" d="M13.6061 6.63762C14.1243 7.15581 14.1243 7.99596 13.6061 8.51415L8.54446 13.5758C8.02628 14.094 7.18613 14.094 6.66794 13.5758L1.60627 8.51415C1.08809 7.99596 1.08809 7.15581 1.60627 6.63762C2.12446 6.11944 2.96461 6.11944 3.4828 6.63762L7.6062 10.761L11.7296 6.63762C12.2478 6.11944 13.0879 6.11944 13.6061 6.63762Z" fill="white"/>
                </svg>
            </div>
            <span class="select__error" data-field-error="true"></span>
          </div>
          <div class="select__dropdown">
            <ul class="select__list">
              {{items}}
            </ul>
          </div>
        `,
        item: `<li class="select__item {{className}}" data-type="item" data-id="{{id}}">{{value}}</li>`,
    },
    data: selectArr,
});

const popupVacancy = new Popup({
    popup: "[data-popup-vacancy]",
    trigger: "[data-popup-vacancy-trigger]",
    lockScroll: true,
    form: true,
    onOpen: (event) => {
        // const popup = event.detail.popup;
        const trigger = event.detail.trigger;

        const vacancyID = trigger.getAttribute("data-popup-vacancy-trigger");
        if (vacancyID) {
            vacancySelect.select(vacancyID);
        }
    },
    onClose: (event) => {
        // const popup = event.detail.popup;
    },
});

const popupWorth = new Popup({
    popup: "[data-popup-worth]",
    trigger: "[data-card-worth]",
    lockScroll: true,
    onlyMob: true,
    onOpen: (event) => {
        const popup = event.detail.popup;
        const trigger = event.detail.trigger;

        const cardData = {};
        const cardHeadingEl = trigger.querySelector("[data-card-worth-heading]");
        if (cardHeadingEl) cardData.heading = cardHeadingEl.innerHTML;
        const cardDescEl = trigger.querySelector("[data-card-worth-desc]");
        if (cardDescEl) cardData.desc = cardDescEl.innerHTML;

        const worthContainer = popup.querySelector("[data-worth-container]");
        if (cardData.heading) {
            worthContainer.insertAdjacentHTML("beforeend", `<div class="h3 mb-16">${cardData.heading}</div>`);
        }
        if (cardData.desc) {
            worthContainer.insertAdjacentHTML("beforeend", `<div class="text-default">${cardData.desc}</div>`);
        }
    },
    onClose: (event) => {
        const popup = event.detail.popup;
        const worthContainer = popup.querySelector("[data-worth-container]");
        worthContainer.innerHTML = "";
    },
});

const popupThanks = new Popup({
    popup: "[data-popup-thanks]",
    trigger: "[data-popup-thanks-trigger]",
    lockScroll: true,
});
const popupError = new Popup({
    popup: "[data-popup-error]",
    trigger: "[data-popup-error-trigger]",
    lockScroll: true,
});
export { popupStory, popupVacancy, popupWorth, popupThanks, popupError };
