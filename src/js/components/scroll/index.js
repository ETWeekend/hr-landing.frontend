import ScrollTriggers from "@components/scroll/ScrollTriggers";

const anchors = new ScrollTriggers({
    trigger: "[data-anchor]",
    padding: 50,
});

export default anchors;
