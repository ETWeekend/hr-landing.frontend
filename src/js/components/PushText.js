class PushText {
    constructor(props) {
        this.elements = props.elements;
        this.pauseTime = props.pauseTime;

        this.init();
    }

    init() {
        const observer = new IntersectionObserver(
            (entries, observer) => {
                entries.forEach((entry) => {
                    if (entry.isIntersecting) {
                        observer.unobserve(entry.target);
                        this.runAnimation(entry.target);
                    }
                });
            },
            {
                rootMargin: "20px 0px",
            }
        );

        this.elements.forEach((element) => {
            if (element.initialized) return;
            observer.observe(element);
            element.initialized = true;
        });
    }

    runAnimation(element) {
        const texts = element.querySelectorAll("span");
        let counter = 1;
        texts.forEach((text) => {
            setTimeout(() => {
                text.classList.add("active");
            }, this.pauseTime * counter);
            counter++;
        });
    }
}

export default PushText;
