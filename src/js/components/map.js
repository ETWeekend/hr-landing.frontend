/* global ymaps */
import { loadScript } from "@utils/helpers";
import { isMobileDevice } from "@utils/devices";

class Map {
    constructor() {
        this.$elements = document.querySelectorAll("[data-ymap]");
        this.$triggers = document.querySelectorAll("[data-ymap-trigger]");
    }

    render() {
        if (isMobileDevice()) {
            const observer = new IntersectionObserver(
                (entries, observer) => {
                    entries.forEach((entry) => {
                        if (entry.isIntersecting) {
                            this.downloadAPI();
                            observer.unobserve(entry.target);
                        }
                    });
                },
                {
                    rootMargin: "20px 0px",
                }
            );

            this.$elements.forEach((element) => {
                observer.observe(element);
            });
        } else {
            this.$triggers.forEach((trigger) => {
                trigger.addEventListener(
                    "mouseenter",
                    () => {
                        this.downloadAPI();
                    },
                    {
                        once: true,
                    }
                );
            });
        }
    }

    downloadAPI() {
        if (window.ymaps) {
            this.init();
        } else {
            loadScript("https://api-maps.yandex.ru/2.1/?lang=ru_RU").then(() => this.init());
        }
    }

    init() {
        this.$elements.forEach((el) => {
            const _target = el;
            if (_target.initialized) return;

            const stateData = el.getAttribute("data-ymap-markers");
            const _state = JSON.parse(stateData);

            const dataOptions = el.getAttribute("data-ymap-options");
            let _options;
            let centerMap;
            let mapZoom;
            let zoomControl = true;
            let suppressMapOpenBlock = false;

            if (dataOptions) {
                _options = JSON.parse(dataOptions);
                if (window.innerWidth < 990) {
                    centerMap = _options.centerMobile || _state[0].position;
                    mapZoom = _options.zoomMobile || 7;
                } else {
                    centerMap = _options.center || _state[0].position;
                    mapZoom = _options.zoom || 13;
                }
                if (_options.zoomControl) {
                    zoomControl = _options.zoomControl;
                }
                suppressMapOpenBlock = _options.suppressMapOpenBlock;
            } else {
                centerMap = _state[0].position;
                mapZoom = 13;
            }

            ymaps.ready(() => {
                const myMap = new ymaps.Map(
                    _target,
                    {
                        center: centerMap,
                        zoom: mapZoom,
                        controls: [],
                    },
                    {
                        suppressMapOpenBlock,
                    }
                );

                myMap.behaviors.disable("scrollZoom");

                if (zoomControl) {
                    myMap.controls.add("zoomControl", {
                        position: {
                            right: "20px",
                            left: "auto",
                            bottom: "60px",
                        },
                    });
                }

                this.addMarkers(myMap, _state);

                _target.addEventListener("fitMap", () => {
                    myMap.container.fitToViewport();
                });

                if (_state.length > 1) {
                    // Автоматическое масштабирование и центрирование карты по нескольким точкам
                    // Если 1 елемент, то к нему устанавливается максимальный zoom
                    // myMap.setBounds(myMap.geoObjects.getBounds());
                }

                const mapID = _target.getAttribute("data-ymap");
                if (mapID) {
                    const sidebar = document.querySelector(`[data-map-sidebar="${mapID}"]`);
                    if (!sidebar) return;
                    const items = sidebar.querySelectorAll("[data-map-sidebar-item]");
                    items.forEach((item) => {
                        item.addEventListener("click", () => {
                            if (item.classList.contains("active")) return;

                            items.forEach((el) => el.classList.remove("active"));
                            item.classList.add("active");

                            const param = item.getAttribute("data-map-sidebar-item");
                            this.filterBy(myMap, _state, param);
                        });
                    });
                }
            });
            _target.initialized = true;
        });
    }

    addMarkers(map, arr) {
        if (arr.length === 1) {
            map.geoObjects.removeAll();
            map.geoObjects.add(this.getPlacemark(arr[0]));
        } else {
            let geoObjects = [];
            for (let i = 0; i < arr.length; i++) {
                // map.geoObjects.add(this.getPlacemark(arr[i])); // Без кластера
                geoObjects[i] = this.getPlacemark(arr[i]);
            }
            this.addClusterer(map, geoObjects);
        }
    }

    addClusterer(map, geoObjects) {
        const myClusterIconContentLayout = ymaps.templateLayoutFactory.createClass(
            '<div class="map-cluster"><div class="map-cluster__number">{{ properties.geoObjects.length }}</div></div>'
        );

        const cluster = new ymaps.Clusterer({
            clusterIcons: [
                {
                    href: "assets/images/map/marker-cluster.svg",
                    size: [44, 44],
                    offset: [-22, -22],
                },
            ],
            clusterIconContentLayout: myClusterIconContentLayout,
            margin: 90,
            zoomMargin: 90,
            hasHint: false,
            gridSize: 64,
            maxZoom: 17,
        });

        cluster.add(geoObjects);
        map.geoObjects.removeAll();
        map.geoObjects.add(cluster);
    }

    getPlacemark(marker) {
        let customBalloonContent, hasBalloon;
        if (marker.customBalloon) {
            if (marker.customBalloon.light) {
                customBalloonContent = `<div class="text-small color-graphite">${marker.customBalloon.label}</div>`;
            } else {
                customBalloonContent = `<div class="custom-balloon">
                                    <div class="custom-balloon__content">
                                        <div class="custom-balloon__label">${marker.customBalloon.label}</div>
                                    </div>
                                </div>`;
            }
            hasBalloon = true;
        } else {
            customBalloonContent = "";
            hasBalloon = false;
        }

        const objectPlacemark = new ymaps.Placemark(
            marker.position,
            {
                balloonContentBody: customBalloonContent,
            },
            {
                iconLayout: "default#image",
                iconImageHref: marker.iconImageHref,
                iconImageSize: marker.iconImageSize,
                iconImageOffset: marker.iconImageOffset,
                hasBalloon,
                balloonOffset: marker.balloonOffset || [0, -marker.iconImageSize[1]],
                hideIconOnBalloonOpen: false,
                balloonMaxWidth: 568,
            }
        );

        return objectPlacemark;
    }

    filterBy(map, markers, param) {
        let filteredMarkers = [];
        for (let i = 0; i < markers.length; i++) {
            let marker = markers[i];
            if (marker.param === param || marker.param === "main" || param === "all") {
                filteredMarkers.push(marker);
            }
        }
        this.addMarkers(map, filteredMarkers);
    }
}

const map = new Map();
export default map;
