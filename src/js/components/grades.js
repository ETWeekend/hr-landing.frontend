import { gsap } from "gsap";
import { ScrollTrigger } from "gsap/ScrollTrigger";
gsap.registerPlugin(ScrollTrigger);

const gradesChanges = () => {
    // const rx = window.innerWidth < 1000 ? window.innerWidth / 1200 : 1;
    // const ry = window.innerHeight < 700 ? window.innerHeight / 1200 : 1;

    const container = document.querySelector("[data-grades-wrapper]");
    if (!container) return false;

    const text = container.querySelector("[data-grades-title]");
    const svg = container.querySelector("[data-grades-svg]");

    ScrollTrigger.create({
        trigger: container,
        start: "top top",
        end: "bottom+=100vh bottom",
        pin: true,
        pinType: "fixed",
        markers: false,
        pinReparent: true,
        pinSpacing: false,
        onUpdate: (self) => {
            const progress = parseFloat(self.progress.toFixed(1));

            if (window.innerWidth > 621) {
                if (progress > 0.2) svg.style.transform = `scale(${progress * 1.1})`;
                else svg.style.transform = `scale(0.25)`;
            } else {
                if (progress > 0.4) svg.style.transform = `scale(${progress * 0.8})`;
                else svg.style.transform = `scale(0.35)`;
            }

            progress > 0 && progress < 0.5
                ? (text.innerText = "JUNIOR")
                : progress > 0.5 && progress < 1
                ? (text.innerText = "MIDDLE")
                : progress >= 1
                ? (text.innerText = "SENIOR")
                : null;
        },
    });
};

export default gradesChanges;
