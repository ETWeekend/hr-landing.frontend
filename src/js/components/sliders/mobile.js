import Slider from "@components/sliders/Slider";

export const sliderMobile = new Slider({
    wrap: "[data-slider-mobile-wrap]",
    slider: "[data-slider-mobile]",
    bullets: true,
    options: {
        slidesPerView: 1,
        spaceBetween: 20,
        speed: 700,
        autoHeight: true,
    },
});
