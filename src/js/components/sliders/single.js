import Slider from "@components/sliders/Slider";

export const sliderSingle = new Slider({
    wrap: "[data-slider-single-wrap]",
    slider: "[data-slider-single]",
    prev: "[data-nav-arrow-prev]",
    next: "[data-nav-arrow-next]",
    bullets: true,
    options: {
        slidesPerView: 1,
        spaceBetween: 20,
        speed: 700,
        loop: true,
        autoHeight: true,
        on: {
            setTransition: (e) => {
                window.dispatchEvent(new Event("cursor:leave"));
            },
        },
    },
});
