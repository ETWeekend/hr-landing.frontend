import Swiper from "swiper";

class Slider {
    constructor(props) {
        this.wrap = props.wrap;
        this.slider = props.slider;
        this.prev = props.prev;
        this.next = props.next;
        this.disabledClass = props.disabledClass || "disabled";
        this.counter = props.counter;
        this.counterExtra = props.counterExtra;
        this.bullets = props.bullets;
        this.options = props.options;
        this.updateListener = props.updateListener;
    }

    init() {
        const wraps = document.querySelectorAll(this.wrap);

        for (let i = 0; i < wraps.length; i++) {
            const wrap = wraps[i];
            const sliderSelector = wrap.querySelector(this.slider);

            if (sliderSelector.swiper) continue;

            const prev = wrap.querySelectorAll(this.prev);
            const next = wrap.querySelectorAll(this.next);

            if (prev !== undefined && next !== undefined) {
                this.options.navigation = {
                    prevEl: prev,
                    nextEl: next,
                    disabledClass: this.disabledClass,
                };
            }

            if (this.bullets) {
                const bulletsSelector = wrap.querySelector("[data-slider-pagination]");
                this.options.pagination = {
                    el: bulletsSelector,
                    type: "bullets",
                    clickable: true,
                };
            }

            const sliderInstance = new Swiper(sliderSelector, this.options);

            if (this.updateListener) {
                sliderSelector.addEventListener("updateSlider", () => {
                    sliderInstance.update();
                });
            }
        }
    }
}

export default Slider;
