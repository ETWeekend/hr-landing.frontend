import { debounce, throttle, getScrollbarWidth } from "@utils/helpers";

class Header {
    constructor() {
        this.header = document.querySelector("[data-header]");
        this.body = document.body;
        this.scrollPosition = 0;
        this.isMenuOpened = false;
    }

    init() {
        if (!this.header) return;
        this.isHeaderAlreadyWhite = this.header.classList.contains("header--white");

        this.navItems = this.header.querySelectorAll("[data-nav-menu-item]");
        this.menuBottom = this.header.querySelector("[data-header-menu-bottom]");

        this.menuSwitch = document.querySelector("[data-menu-switch]");
        if (this.menuSwitch) {
            this.navMenu();
            this.scrollOperations();
        }
    }

    navMenu() {
        this.menuSwitch.addEventListener("click", () => {
            if (this.isMenuOpened) {
                this.closeMenu();
            } else {
                this.openMenu();
            }
        });

        document.addEventListener("keyup", (event) => {
            if (event.key === "Escape" || event.key === "Esc") {
                this.closeMenu();
            }
        });

        const anchors = this.header.querySelectorAll("[data-anchor]");
        if (anchors.length) {
            anchors.forEach((anchor) => {
                anchor.addEventListener("click", () => {
                    this.closeMenu();
                });
            });
        }
    }

    openMenu() {
        const scrollBarWidth = getScrollbarWidth();
        if (this.header) this.header.style.right = `${scrollBarWidth}px`;
        document.documentElement.style.marginRight = `${scrollBarWidth}px`;
        document.documentElement.style.overflow = "hidden";

        this.body.classList.add("menu-opened");
        this.isMenuOpened = true;

        const delay = 150;
        let delayCounter = 1;
        this.navItems.forEach((item) => {
            item.style.setProperty("transition-delay", delayCounter * delay + "ms");
            item.style.setProperty("pointer-events", "none");
            delayCounter++;
        });
        this.menuBottom.style.setProperty("transition-delay", delayCounter * delay + "ms");

        this.counterTimeout = setTimeout(() => {
            this.navItems.forEach((item) => {
                item.style.removeProperty("transition-delay");
                item.style.removeProperty("pointer-events");
            });
            this.menuBottom.style.removeProperty("transition-delay");
        }, delayCounter * delay);
    }

    closeMenu() {
        this.body.classList.remove("menu-opened");
        this.isMenuOpened = false;

        if (this.header) this.header.style.removeProperty("right");
        document.documentElement.style.removeProperty("margin-right");
        document.documentElement.style.removeProperty("overflow");

        this.navItems.forEach((item) => {
            item.style.removeProperty("transition-delay");
        });

        clearTimeout(this.counterTimeout);
    }

    scrollOperations() {
        const layers = document.querySelectorAll("[data-color-layer]");

        const scrollHandler = () => {
            this.scrollFromZero();
            this.scrollDirection();
            this.scrollColor(layers);
        };
        scrollHandler();

        const resizeHandler = () => {
            this.scrollColor(layers);
        };

        window.addEventListener("scroll", throttle(scrollHandler, 15));
        window.addEventListener("resize", debounce(resizeHandler, 100));
    }

    scrollFromZero() {
        if (window.pageYOffset > 4) {
            this.body.classList.add("scrolled-from-zero");
        } else {
            this.body.classList.remove("scrolled-from-zero");
        }
    }

    scrollDirection() {
        if (window.pageYOffset <= this.scrollPosition) {
            this.body.classList.remove("scroll-down"); // up
        } else {
            this.body.classList.add("scroll-down"); // down
        }
        this.scrollPosition = window.pageYOffset;
    }

    scrollColor(layers) {
        let isSetHeaderWhite;
        const measurementLine = this.getMeasurementLine();

        for (let i = 0; i < layers.length; i++) {
            const start = layers[i].getBoundingClientRect().top - measurementLine;
            const end = start + layers[i].offsetHeight;
            if (start <= 0 && end >= 0) {
                isSetHeaderWhite = true;
                break;
            } else {
                isSetHeaderWhite = false;
            }
        }

        if (this.isHeaderAlreadyWhite !== isSetHeaderWhite) {
            if (isSetHeaderWhite) {
                this.header.classList.add("header--white");
            } else {
                this.header.classList.remove("header--white");
            }
        }

        this.isHeaderAlreadyWhite = isSetHeaderWhite;
    }

    getMeasurementLine() {
        return Math.round(this.menuSwitch.offsetHeight / 2 + this.menuSwitch.getBoundingClientRect().top);
    }
}

export default new Header();
