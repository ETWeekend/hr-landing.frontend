import Accordion from "@components/accordion/Accordion";

const accordionDefault = new Accordion({
    wrap: "[data-accordion]",
    row: "[data-accordion-row]",
    trigger: "[data-accordion-trigger]",
    content: "[data-accordion-content]",
    closeAll: true,
});

export { accordionDefault };
