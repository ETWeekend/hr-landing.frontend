class Accordion {
    constructor(props) {
        this.wrap = props.wrap;
        this.trigger = props.trigger;
        this.row = props.row;
        this.content = props.content;
        this.closeName = "closed";
        this.openName = "opened";
        this.duration = 300;
        this.closeAll = props.closeAll;
    }

    init() {
        const wraps = document.querySelectorAll(this.wrap);
        if (!wraps.length) return;

        wraps.forEach((wrap) => {
            const rows = wrap.querySelectorAll(this.row);
            for (let i = 0; i < rows.length; i++) {
                const row = rows[i];
                const trigger = row.querySelector(this.trigger);
                const content = row.querySelector(this.content);

                row.state = this.closeName;
                this.reset(content);

                trigger.addEventListener("click", () => {
                    if (row.state === this.closeName) {
                        if (this.closeAll) {
                            rows.forEach((item) => {
                                if (item.state === this.openName) {
                                    this.close(item, item.querySelector(this.content));
                                }
                            });
                        }

                        this.open(row, content);
                    } else {
                        this.close(row, content);
                    }
                });
            }
        });
    }

    open(row, content) {
        row.state = this.openName;
        row.classList.add(this.openName);

        content.style.removeProperty("display");
        const height = content.offsetHeight;
        content.style.overflow = "hidden";
        content.style.height = "0";
        content.style.paddingTop = 0;
        content.style.paddingBottom = 0;
        content.offsetHeight;
        content.style.transitionProperty = "height, padding";
        content.style.transitionDuration = `${this.duration}ms`;
        content.style.height = `${height}px`;
        content.style.removeProperty("padding-top");
        content.style.removeProperty("padding-bottom");

        setTimeout(() => {
            content.style.removeProperty("height");
            content.style.removeProperty("overflow");
            content.style.removeProperty("transition-duration");
            content.style.removeProperty("transition-property");
        }, this.duration);
    }

    close(row, content) {
        row.state = this.closeName;
        row.classList.remove(this.openName);

        content.style.transitionProperty = "height, padding";
        content.style.transitionDuration = `${this.duration}ms`;
        content.style.height = `${content.offsetHeight}px`;
        content.offsetHeight;
        content.style.overflow = "hidden";
        content.style.height = "0";
        content.style.paddingTop = 0;
        content.style.paddingBottom = 0;

        setTimeout(() => {
            content.style.display = "none";
            content.style.removeProperty("height");
            content.style.removeProperty("padding-top");
            content.style.removeProperty("padding-bottom");
            content.style.removeProperty("overflow");
            content.style.removeProperty("transition-duration");
            content.style.removeProperty("transition-property");
        }, this.duration);
    }

    reset(content) {
        content.style.display = "none";
    }
}

export default Accordion;
