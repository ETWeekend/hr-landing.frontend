class FileUpload {
    constructor(props) {
        this.selector = props.selector;
    }

    init() {
        const fields = document.querySelectorAll(this.selector);

        fields.forEach((field) => {
            const label = field.querySelector("label");
            const input = field.querySelector("input[type=file]");

            const placeholder = label.innerText;
            input.addEventListener("change", () => {
                if (!input.files.length) return (label.innerText = placeholder);

                let newLabel;
                if (input.files.length > 1) {
                    newLabel = `Выбрано файлы (${input.files.length})`;
                } else {
                    newLabel = input.files[0].name;
                }

                label.innerText = newLabel;
            });
        });
    }
}

const fileUpload = new FileUpload({
    selector: "[data-file-upload]",
});

export default fileUpload;
