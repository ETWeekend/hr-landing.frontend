import { MultiSelect } from "./Multiple";
import { SingleSelect } from "./Single";

export { MultiSelect, SingleSelect };
