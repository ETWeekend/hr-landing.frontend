import { Select } from "./install";

export class SingleSelect extends Select {
    constructor(selector, options) {
        super(selector, options);

        this._current = [];
    }
}
