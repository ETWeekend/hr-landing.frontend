import { Select } from "./install";

const removeItem = (arr, id) => {
    arr.some((item, idx) => {
        return arr[idx].id == parseInt(id) ? !!arr.splice(idx, 1) : false;
    });
    return arr;
};

export class MultiSelect extends Select {
    constructor(selector, options) {
        super(selector, options);

        this.renderId = options.renderId;
    }

    select(id) {
        this.selectedId = id;
        const optionData = this.options.data;
        const current = this.current;

        if (this.options.selectedAll !== undefined && this.selectedId == this.options.selectedAll) {
            this._current = [];
            this.$el.querySelectorAll(`[data-id]`).forEach((el) => {
                el.classList.remove("selected");
            });
            if (this.$el.querySelector(`[data-id="${this.selectedId}"]`)) {
                this.$el.querySelector(`[data-id="${this.selectedId}"]`).classList.add("selected");
            }

            this._current.push(current);
        } else {
            if (this.options.selectedAll !== undefined && typeof this.options.selectedAll === "number") {
                if (this.$el.querySelector(`[data-id="${this.options.selectedAll}"]`)) {
                    this.$el.querySelector(`[data-id="${this.options.selectedAll}"]`).classList.remove("selected");
                    removeItem(this._current, parseInt(this.options.selectedAll));
                }
            }

            if (this._current.length <= 1) {
                removeItem(this._current, parseInt(this.selectedId));
                if (this.$el.querySelector(`[data-id="${this.selectedId}"]`)) {
                    this.$el.querySelector(`[data-id="${this.selectedId}"]`).classList.add("selected");
                }

                optionData.filter((item) => {
                    if (item.id == this.selectedId) {
                        this._current.push(item);
                    }
                });
            } else {
                this.$el.querySelector(`[data-id="${id}"]`).classList.toggle("selected");
                this.$el.querySelectorAll(`[data-id]`).forEach((el) => {
                    if (!el.classList.contains("selected")) {
                        removeItem(this._current, parseInt(el.dataset.id));
                    } else {
                        const currentId = parseInt(el.dataset.id);
                        if (currentId == this.current.id) {
                            optionData.filter((item) => {
                                if (item.id == currentId) {
                                    this._current.push(item);
                                }
                            });
                        }
                    }
                });
            }
        }

        this.$value.textContent = "";
        this.$input ? (this.$input.value = "") : "";
        this._current.forEach((render) => {
            this.$value.textContent += `${render.value}, `;
            if (this.renderId === true) {
                this.$input ? (this.$input.value += `${render.id},`) : "";
            } else {
                this.$input ? (this.$input.value += `${render.value},`) : "";
            }
        });
        this.$input.value = this.$input.value.substring(0, this.$input.value.lastIndexOf(","));

        if (this._current.length === 0) {
            this.$value.textContent = this.options.placeholder;
            this.$input ? (this.$input.value = "") : "";
        }

        this.options.onSelect ? this.options.onSelect(this._current) : null;
    }

    dispatch(id) {
        this.selectedId = id;
        const optionData = this.options.data;
        const current = this.current;

        if (this.options.selectedAll !== undefined && this.selectedId == this.options.selectedAll) {
            this._current = [];
            this.$el.querySelectorAll(`[data-id]`).forEach((el) => {
                el.classList.remove("selected");
            });
            if (this.$el.querySelector(`[data-id="${this.selectedId}"]`)) {
                this.$el.querySelector(`[data-id="${this.selectedId}"]`).classList.add("selected");
            }

            this._current.push(current);
        } else {
            if (this.options.selectedAll !== undefined && typeof this.options.selectedAll === "number") {
                if (this.$el.querySelector(`[data-id="${this.options.selectedAll}"]`)) {
                    this.$el.querySelector(`[data-id="${this.options.selectedAll}"]`).classList.remove("selected");
                    removeItem(this._current, parseInt(this.options.selectedAll));
                }
            }

            if (this._current.length <= 1) {
                removeItem(this._current, parseInt(this.selectedId));
                if (this.$el.querySelector(`[data-id="${this.selectedId}"]`)) {
                    this.$el.querySelector(`[data-id="${this.selectedId}"]`).classList.add("selected");
                }

                optionData.filter((item) => {
                    if (item.id == this.selectedId) {
                        this._current.push(item);
                    }
                });
            } else {
                this.$el.querySelector(`[data-id="${id}"]`).classList.toggle("selected");
                this.$el.querySelectorAll(`[data-id]`).forEach((el) => {
                    if (!el.classList.contains("selected")) {
                        removeItem(this._current, parseInt(el.dataset.id));
                    } else {
                        const currentId = parseInt(el.dataset.id);
                        if (currentId == this.current.id) {
                            optionData.filter((item) => {
                                if (item.id == currentId) {
                                    this._current.push(item);
                                }
                            });
                        }
                    }
                });
            }
        }

        this.$value.textContent = "";
        this.$input ? (this.$input.value = "") : "";
        this._current.forEach((render) => {
            this.$value.textContent += `${render.value}, `;
            if (this.renderId === true) {
                this.$input ? (this.$input.value += `${render.id},`) : "";
            } else {
                this.$input ? (this.$input.value += `${render.value},`) : "";
            }
        });
        this.$input.value = this.$input.value.substring(0, this.$input.value.lastIndexOf(","));

        if (this._current.length === 0) {
            this.$value.textContent = this.options.placeholder;
            this.$input ? (this.$input.value = "") : "";
        }

        // this.options.onSelect ? this.options.onSelect(this._current) : null;
    }
}
