const getTemplate = (data = [], placeholder, selectedId, customTemplate) => {
    let text = placeholder ?? "Placeholder по умолчанию";
    let id = 0;

    if (!!customTemplate) {
        let _item = customTemplate.item,
            _container = customTemplate.container;

        const items = data.map((item) => {
            let cls = "";
            if (item.id == selectedId) {
                text = item.value;
                id = item.id;
                cls = "selected";
            }

            return _item
                .replace(/\{{className}}/gm, cls)
                .replace(/\{{id}}/gm, item.id)
                .replace(/\{{value}}/gm, item.value);
        });

        return _container
            .replace(/\{{id}}/gm, id)
            .replace(/\{{value}}/gm, text)
            .replace(/\{{selected}}/gm, text !== placeholder ? text : "")
            .replace(/\{{items}}/gm, items.join(""));
    } else {
        const items = data.map((item) => {
            let cls = "";
            if (item.id === selectedId) {
                text = item.value;
                cls = "selected";
            }
            return `
          <li class="select__item ${cls}" data-type="item" data-id="${item.id}">${item.value}</li>
        `;
        });

        return `
        <div class="select__backdrop" data-type="backdrop"></div>
        <div class="select__input" data-type="input">
          <div class="select__value select__value--placeholder" data-type="value">${text}</div>
        </div>
        <div class="select__dropdown">
          <ul class="select__list">
            ${items.join("")}
          </ul>
        </div>
      `;
    }
};

export class Select {
    constructor(selector, options) {
        this.$el = document.querySelector(selector);
        this.options = options;
        this._current = []; // TODO: Для мультиселекта
        this.selectedId = options.selectedId;
        options.data.filter((el) => {
            // TODO: Нужно для того, если выбран ID не по selectedAll, чтобы значение остовалось активным
            if (el.id === this.selectedId) this._current.push(el);
        });

        this.renderId = options.renderId;

        this.#render();
        this.#setup();
    }

    #render(arr) {
        let { placeholder, data } = this.options;
        if (!!arr) data = arr;

        if (!!this.$el) {
            this.$el.classList.add("select");
            if (data && data.length < 1) {
                this.$el.classList.add("disabled");
            }

            this.$el.innerHTML = getTemplate(data, placeholder, this.selectedId, this.options.template);
        }
    }

    #setup() {
        if (!!this.$el) {
            this.clickHandler = this.clickHandler.bind(this);
            this.$el.addEventListener("click", this.clickHandler);
            this.$arrow = this.$el.querySelector('[data-type="arrow"]');
            this.$value = this.$el.querySelector('[data-type="value"]');
            this.$input = this.$el.querySelector('input[type="hidden"]');
        }
    }

    clickHandler(event) {
        const { type } = event.target.dataset;

        if (type === "input") {
            this.toggle();
        } else if (type === "value") {
            this.toggle();
        } else if (type === "item") {
            const id = parseInt(event.target.dataset.id);
            this.select(id);
        } else if (type === "backdrop") {
            this.close();
        }
    }

    get isOpen() {
        return this.$el.classList.contains("open");
    }

    get current() {
        return this.options.data.find((item) => item.id == this.selectedId);
    }

    select(id) {
        this.selectedId = id;
        this.$value.innerHTML = this.current.value;
        this.$input ? (this.$input.value = this.current.value) : "";

        if (this.renderId === true) {
            this.$input ? (this.$input.value = `${this.current.id}`) : "";
        } else {
            this.$input ? (this.$input.value = `${this.current.value}`) : "";
        }

        this.$el.querySelectorAll('[data-type="item"]').forEach((el) => {
            el.classList.remove("selected");
        });
        if (!!this.$el.querySelector(`[data-id="${id}"]`)) {
            this.$el.querySelector(`[data-id="${id}"]`).classList.add("selected");
            this.$el.querySelector(".select__value").classList.remove("select__value--placeholder");
        }

        this.options.onSelect ? this.options.onSelect(this.current) : null;

        this.close();
    }

    dispatch(id) {
        this.selectedId = id;

        this.$value.innerHTML = this.current.value;
        this.$input ? (this.$input.value = this.current.value) : "";

        if (this.renderId === true) {
            this.$input ? (this.$input.value = `${this.current.id}`) : "";
        } else {
            this.$input ? (this.$input.value = `${this.current.value}`) : "";
        }

        this.$el.querySelectorAll('[data-type="item"]').forEach((el) => {
            el.classList.remove("selected");
        });
        if (!!this.$el.querySelector(`[data-id="${id}"]`)) {
            this.$el.querySelector(`[data-id="${id}"]`).classList.add("selected");
        }
        this.close();
    }

    toggle() {
        this.isOpen ? this.close() : this.open();
    }

    disable() {
        this.$el.classList.add("disabled");
    }
    enable() {
        this.$el.classList.remove("disabled");
    }

    open() {
        this.$el.classList.add("open");
    }

    close() {
        this.$el.classList.remove("open");
    }

    destroy() {
        const listener = this.clickHandler;
        this.$el.removeEventListener("click", listener);

        this.$el.innerHTML = "";
    }

    reinit(data) {
        if (!!data) {
            this.options.data = data;
        }
        if (data[0].value !== "Неважно" && data[0].value !== "Сдан") {
            this.selectedId = data[0].id;
        }

        this._current = [];

        this.destroy();

        setTimeout(() => {
            this.#render();
            this.#setup();
        });
    }
}
