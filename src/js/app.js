import { isMobile } from "@utils/devices";

import header from "@components/header";
import anchors from "@components/scroll";
import CheckCookies from "@components/cookie-message";

import { sliderSingle } from "@components/sliders/single";
import { sliderMobile } from "@components/sliders/mobile";
import { accordionDefault } from "@components/accordion";
import { popupStory, popupVacancy, popupWorth } from "@components/popup/index";
import map from "@components/map";
// import "@prismic/hoverEffect/index";

import gradesChanges from "@components/grades";
// import skewVelocityOfScroll from "@prismic/skewScroll";
import Cursor from "@prismic/cursor";

import Forms from "@components/forms/forms";
import fileUpload from "@components/fileUpload";

document.addEventListener("DOMContentLoaded", () => {
    new Cursor();
    new CheckCookies();

    header.init();
    anchors.init();
    sliderSingle.init();
    if (isMobile()) {
        sliderMobile.init();
    }
    accordionDefault.init();
    fileUpload.init();

    popupStory.init();
    popupVacancy.init();
    popupWorth.init();

    gradesChanges();
    // skewVelocityOfScroll();

    map.render();
});
