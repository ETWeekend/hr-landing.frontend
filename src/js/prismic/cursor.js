const lerp = (a, b, n) => (1 - n) * a + n * b;

class Cursor {
    constructor() {
        this.target = {
            x: 0.5,
            y: 0.5,
        };
        this.cursor = {
            x: 0.5,
            y: 0.5,
        };
        this.speed = 0.2;
        this.cursorEl = document.querySelector(".cursor");

        this.init();
        this.transformBlob();
    }

    randomNumber(min, max) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }
    transformBlob() {
        let scaleX = this.randomNumber(25, 150);
        let scaleY = this.randomNumber(25, 150);
        let rotate = this.randomNumber(-180, 180);

        const svg = document.querySelector(".cursor svg");
        svg.style.fill = "#fff";
        svg.style.transform = "scale3d(" + scaleX / 100 + "," + scaleY / 100 + ",1) rotate(" + rotate + "deg)";
    }
    bindAll() {
        ["onMouseMove", "render"].forEach((fn) => (this[fn] = this[fn].bind(this)));
    }
    onMouseMove(e) {
        this.target.x = e.clientX / window.innerWidth;
        this.target.y = e.clientY / window.innerHeight;

        if (!this.raf) {
            this.raf = requestAnimationFrame(this.render);
        }

        const card = e.target.nodeName === "#document" ? null : e.target.closest("[data-cursor-hover]");
        if (card) {
            this.cursorEl.style.setProperty("opacity", "1");
            this.onMouseEnter(card.dataset.cursorText);
        } else {
            this.cursorEl.style.setProperty("opacity", "0");
            this.onMouseLeave();
        }

        if (!!card && card.closest("[data-cursor-get-offset]")) {
            const width = card.offsetWidth,
                height = card.offsetHeight;

            const offsetTop = +card.closest("[data-cursor-get-offset]").offsetTop;
            const padding = 50;
            if (
                e.offsetX <= padding ||
                e.offsetY <= padding ||
                e.offsetX >= width - padding ||
                e.offsetY >= height - padding
            ) {
                this.cursorEl.style.setProperty("transform", "scale(0.5)");
            } else {
                this.cursorEl.style.transform = "";
            }
        }
    }
    onMouseEnter(cursorText) {
        const container = document.querySelector(".cursor"),
            text = container.querySelector(".cursor__text");

        if (!!cursorText && cursorText.length !== 0) text.innerText = cursorText;
        container.classList.add("-enter");
    }
    onMouseLeave() {
        const container = document.querySelector(".cursor"),
            text = container.querySelector(".cursor__text");

        container.classList.remove("-enter");
        text.innerText = "Читать";
    }
    render() {
        this.cursor.x = lerp(this.cursor.x, this.target.x, this.speed);
        this.cursor.y = lerp(this.cursor.y, this.target.y, this.speed);
        document.documentElement.style.setProperty("--cursor-x", this.cursor.x);
        document.documentElement.style.setProperty("--cursor-y", this.cursor.y);

        const delta = Math.sqrt(
            Math.pow(this.target.x - this.cursor.x, 2) + Math.pow(this.target.y - this.cursor.y, 2)
        );

        if (delta < 0.001) {
            cancelAnimationFrame(this.raf);
            clearInterval(this.blob);
            this.raf = null;
            return;
        }

        this.raf = requestAnimationFrame(this.render);
    }
    init() {
        this.bindAll();
        window.addEventListener("mousemove", this.onMouseMove);
        window.addEventListener("scroll", this.onMouseLeave);
        window.addEventListener("cursor:leave", this.onMouseLeave);
        this.raf = requestAnimationFrame(this.render);

        this.transformBlob();
        setInterval(() => {
            this.transformBlob();
        }, 1000);
    }
}

export default Cursor;
