import { gsap } from "gsap";
import { ScrollTrigger } from "gsap/ScrollTrigger";
gsap.registerPlugin(ScrollTrigger);

const skewVelocityOfScroll = () => {
    let proxy = {
            skew: 0,
        },
        skewSetter = gsap.quickSetter("section", "skewY", "deg"),
        clamp = gsap.utils.clamp(-2, 2);

    ScrollTrigger.create({
        onUpdate: (self) => {
            let skew = clamp(self.getVelocity() / -20);

            if (Math.abs(skew) > Math.abs(proxy.skew)) {
                proxy.skew = skew;
                gsap.to(proxy, {
                    skew: 0,
                    duration: 0.8,
                    ease: "power3",
                    overwrite: true,
                    onUpdate: () => skewSetter(proxy.skew),
                });
            }
        },
    });

    gsap.set("section", {
        transformOrigin: "right center",
        force3D: true,
    });
};

export default skewVelocityOfScroll;
