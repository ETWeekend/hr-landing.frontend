import imagesLoaded from "imagesLoaded";
import Scene from "./scene";

const scene = new Scene("container");

// helper functions
const MathUtils = {
    // map number x from range [a, b] to [c, d]
    map: (x, a, b, c, d) => ((x - a) * (d - c)) / (b - a) + c,
    // linear interpolation
    lerp: (a, b, n) => (1 - n) * a + n * b,
};

const body = document.body;
let IMAGES;

// расчет область видимости
let winsize;
const calcWinsize = () => (winsize = { width: window.innerWidth, height: window.innerHeight });
calcWinsize();
window.addEventListener("resize", calcWinsize);

window.onbeforeunload = function () {
    window.scrollTo(0, 0);
};

// scroll position and update function
let docScroll;
const getPageYScroll = () => (docScroll = window.pageYOffset || document.documentElement.scrollTop);
window.addEventListener("scroll", getPageYScroll);

// Item
class Item {
    constructor(el, scroll) {
        // the .item element
        this.scroll = scroll;
        this.DOM = { el: el.img };

        this.currentScroll = docScroll;
        this.animated = false;
        this.isBeingAnimatedNow = false;
        this.shouldRollBack = false;
        this.shouldUnRoll = false;
        this.positions = [];

        // ставим начальное значение
        this.getSize();
        this.mesh = scene.createMesh({
            width: this.width,
            height: this.height,
            src: this.src,
            image: this.DOM.el,
            iWidth: this.DOM.el.width,
            iHeight: this.DOM.el.height,
        });
        scene.scene.add(this.mesh);
        // проверяем через observer попал ли элемент в область видимости
        // после этого обновляем элемент
        this.intersectionRatio;
        let options = {
            root: null,
            rootMargin: "0px",
            threshold: [0, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1],
        };
        this.observer = new IntersectionObserver((entries) => {
            entries.forEach((entry) => {
                this.positions.push(entry.boundingClientRect.y);
                let compareArray = this.positions.slice(this.positions.length - 2, this.positions.length);
                let down = compareArray[0] > compareArray[1] ? true : false;

                this.isVisible = entry.intersectionRatio > 0.0;

                this.shouldRollBack = false;
                this.shouldUnRoll = false;
                if (entry.intersectionRatio < 0.5 && entry.boundingClientRect.y > 0 && this.isVisible && !down) {
                    this.shouldRollBack = true;
                }

                if (entry.intersectionRatio > 0.5 && entry.boundingClientRect.y > 0 && this.isVisible) {
                    this.shouldUnRoll = true;
                }

                this.mesh.visible = this.isVisible;
            });
        }, options);
        this.observer.observe(this.DOM.el);

        window.addEventListener("resize", () => this.resize());
        this.render(0);
    }

    getSize() {
        // получение всех размеров и границ
        const bounds = this.DOM.el.getBoundingClientRect();
        const fromTop = bounds.top;
        const windowHeight = window.innerHeight;
        const withoutHeight = fromTop - windowHeight;
        const withHeight = fromTop + bounds.height;
        this.insideTop = withoutHeight - docScroll;
        this.insideRealTop = fromTop + docScroll;
        this.insideBottom = withHeight - docScroll + 50;
        this.width = bounds.width;
        this.height = bounds.height;
        this.left = bounds.left;
    }
    resize() {
        // при изменении размеров и обновление значений
        this.getSize();
        this.mesh.scale.set(this.width, this.height, 200);
        this.render(this.scroll.renderedStyles.translationY.current);
        this.scroll.shouldRender = true;
    }

    render(currentScroll) {
        this.currentScroll = currentScroll;
        this.mesh.position.y = currentScroll + winsize.height / 2 - this.insideRealTop - this.height / 2;
        this.mesh.position.x = 0 - winsize.width / 2 + this.left + this.width / 2;
    }
}

class SmoothScroll {
    constructor() {
        this.shouldRender = false;

        this.DOM = { main: document.querySelector("main") };
        // меняем ось y при скролле
        this.DOM.scrollable = this.DOM.main.querySelector("div[data-scroll]");
        // все элементы на странице
        this.items = [];

        this.createItems();
        this.listenMouse();

        this.renderedStyles = {
            translationY: {
                previous: 0,
                current: 0,
                ease: 0.1,
                setValue: () => docScroll,
            },
        };
        this.update();
        requestAnimationFrame(() => this.render());
    }

    listenMouse() {
        document.addEventListener("mousemove", () => {
            this.shouldRender = true;
        });
    }

    update() {
        for (const key in this.renderedStyles) {
            this.renderedStyles[key].current = this.renderedStyles[key].previous = this.renderedStyles[key].setValue();
        }
        this.setPosition();
        this.shouldRender = true;
    }
    setPosition() {
        if (
            Math.round(this.renderedStyles.translationY.previous) !==
                Math.round(this.renderedStyles.translationY.current) ||
            this.renderedStyles.translationY.previous < 10
        ) {
            this.shouldRender = true;
            for (const item of this.items) {
                if (item.isVisible || item.isBeingAnimatedNow) {
                    item.render(this.renderedStyles.translationY.previous);
                }
            }
        }
        if (scene.targetSpeed > 0.01) this.shouldRender = true;

        if (this.shouldRender) {
            this.shouldRender = false;
            scene.render();
        }
    }

    createItems() {
        IMAGES.forEach((image) => {
            if (image.img.closest("[data-card-img]")) {
                this.items.push(new Item(image, this));
            }
        });
    }

    render() {
        for (const key in this.renderedStyles) {
            this.renderedStyles[key].current = this.renderedStyles[key].setValue();
            this.renderedStyles[key].previous = MathUtils.lerp(
                this.renderedStyles[key].previous,
                this.renderedStyles[key].current,
                this.renderedStyles[key].ease
            );
        }
        this.setPosition();

        // requestAnimationFrame(() => );
        // this.render();
    }
}

// Preload images
const preloadImages = new Promise((resolve, reject) => {
    imagesLoaded(document.querySelectorAll("[data-card-img]"), { background: true }, resolve);
});

preloadImages.then((images) => {
    IMAGES = images.images;
});

const preloadEverything = [preloadImages];

// And then..
Promise.all(preloadEverything).then(() => {
    // Remove the loader
    document.body.classList.remove("loading");
    document.body.classList.add("loaded");
    // Get the scroll position
    getPageYScroll();
    // Initialize the Smooth Scrolling
    new SmoothScroll();
});
