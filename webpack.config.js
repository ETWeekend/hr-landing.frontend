/* eslint-disable no-undef */
const path = require("path");
const fs = require("fs");
const portFinderSync = require("portfinder-sync");

require("babel-polyfill");
const {
    CleanWebpackPlugin
} = require("clean-webpack-plugin");
const CopyWebpackPlugin = require("copy-webpack-plugin");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const TerserPlugin = require("terser-webpack-plugin");
const PrettierPlugin = require("prettier-webpack-plugin");
// const webpackGlsl = require("webpack-glsl-loader");

const PATHS = {
    src: path.resolve(__dirname, "./src"),
    dist: path.resolve(__dirname, "./dist"),
};
const PAGES_DIR = [{
    dir: `${PATHS.src}/html/pages/`,
    files: []
}, {
    dir: `${PATHS.src}/html/stories/`,
    folder: 'stories',
    files: []
}];

PAGES_DIR.forEach(item => item.files = fs.readdirSync(item.dir).filter((fileName) => fileName.endsWith(".twig")));

const FILES = [];
PAGES_DIR.forEach(item => {
    item.files.forEach(file => {
        FILES.push({
            dir: item.dir,
            folder: item.folder,
            file
        })
    })
})

const port = portFinderSync.getPort(3000);
const isDevelopment = process.env.NODE_ENV === "development";
const isProduction = process.env.NODE_ENV === "production";

if (isDevelopment) console.log("Development");
if (isProduction) console.log("Production");

const config = {
    entry: ["babel-polyfill", "./src/js/app.js", "./src/styles/global.scss"],
    output: {
        path: PATHS.dist,
        filename: "./js/bundle.js",
    },
    devServer: {
        host: "0.0.0.0",
        contentBase: path.resolve(__dirname, "dist"),
        compress: isProduction,
        // open: true,
        port: port,
        public: `localhost:${port}`,
        overlay: {
            warnings: false,
            errors: true,
        },
        historyApiFallback: true,
        inline: true,
    },
    devtool: "source-map",
    mode: "production",
    optimization: {
        minimizer: isProduction ? [
            new TerserPlugin({
                sourceMap: true,
                extractComments: true,
            }),
        ] : []
    },
    resolve: {
        alias: {
            "@components": path.join(__dirname, "src/js/components"),
            "@utils": path.join(__dirname, "src/js/utils"),
            "@libs": path.join(__dirname, "src/js/libs"),
            "@prismic": path.join(__dirname, "src/js/prismic")
        }
    },
    module: {
        rules: [{
            test: /\.twig$/,
            use: [
                "raw-loader",
                {
                    loader: "twig-html-loader",
                    options: {
                        namespaces: {
                            layouts: "./src/html/include/layout",
                            images: "./src/assets/images",
                            components: "./src/html/include/components",
                            blocks: "./src/html/include/blocks",
                        },
                    },
                },
            ],
        },
            {
                test: /\.(png|jpg|svg|gif)$/,
                loader: "file-loader",
            },
            {
                test: /\.(woff|woff2|eot|ttf|otf)$/,
                loader: "file-loader",
            },
            {
                test: /\.(sass|scss|css)$/,
                include: path.resolve(__dirname, "src/styles"),
                use: [
                    MiniCssExtractPlugin.loader,
                    {
                        loader: "css-loader",
                        options: {
                            sourceMap: true,
                            url: false,
                        },
                    },
                    {
                        loader: "postcss-loader",
                        options: {
                            ident: "postcss",
                            sourceMap: true,
                            plugins: () => [
                                require("cssnano")({
                                    preset: [
                                        "default",
                                        {
                                            discardComments: {
                                                removeAll: true,
                                            },
                                        },
                                    ],
                                }),
                                require("autoprefixer")({
                                    grid: true,
                                }),
                            ],
                        },
                    },
                    {
                        loader: "sass-loader",
                        options: {
                            sourceMap: true,
                        },
                    },
                ],
            },
            {
                test: /\.glsl$/,
                loader: "webpack-glsl-loader"
            },
            {
                test: /\.js$/,
                exclude: /(node_modules|bower_components)/,
                use: {
                    loader: "babel-loader",
                    options: {
                        presets: ["@babel/preset-env"],
                    },
                },
            },
        ],
    },
    plugins: [
        new PrettierPlugin({
            printWidth: 120,
            tabWidth: 4,
            useTabs: false,
            semi: true,
            encoding: "utf-8",
            extensions: [".js", ".ts"],
        }),
        new MiniCssExtractPlugin({
            filename: "./css/bundle.css",
        }),
        new CopyWebpackPlugin([
            {
                from: "./src/assets",
                to: "./assets",
            },
            {
                from: "./src/fonts",
                to: "./fonts",
            },
        ]),
        ...FILES.map(page => {
            const filePath = `${page.file.replace(/\.twig/, ".html")}`
            return new HtmlWebpackPlugin({
                template: `${page.dir}${page.file}`,
                filename: page.folder ? `./${page.folder}/${filePath}` : `./${filePath}`,
                inject: false,
                minify: false
            })
        })
    ],
};

module.exports = (env, argv) => {
    if (isProduction) {
        config.plugins.push(new CleanWebpackPlugin());
    }
    return config;
};